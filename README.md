# BL618-G0 评估板 V1.1.2

- [BL618-G0 评估板 V1.1.2](#bl618-g0-评估板-v112)
	- [简介](#简介)
	- [硬件版本历史](#硬件版本历史)
		- [v1.1 与 v1.0](#v11-与-v10)
	- [引脚分配](#引脚分配)
	- [CK-Link 固件](#ck-link-固件)
	- [开发资源](#开发资源)

## 简介

***此为 BL618 评估板 v1.1.2 版的开发资源。现正持续更新中。  
BL618评估板各个版本之间在电源、IO引出方面有一些不同。具体硬件版本修改信息见[硬件版本历史章节](#硬件版本历史)***

![***BL618 评估板***](./doc/pic/BL618eval-top.jpg)

BL618-G0 评估板。  
**原理图、尺寸图、3D模型、CK-Link 固件**等资料见 [***开发资源章节***](#开发资源)  
[摄像头+音频，触摸屏拓展板](https://gitee.com/verimaker/BL618-G0-Audio-Video-Board)  
VeriMake 会在 [***Bilibili 账号***](https://space.bilibili.com/356383684) 上发布相关的教程、应用案例视频。  
VeriMake 论坛有 [***博流智能版块***](https://verimake.com/t/BouffaloLab) 供开发者发帖展示相关项目、分享开发经验、提出开发过程中遇到问题等。  
相关硬件可以从 [***VeriMake的淘宝店***](https://verimake.taobao.com/) 购买。  
![***二维码***](./doc/pic/BiliTao-QR-Code-0.15-fillet.jpg)

## 硬件版本历史

![***BL618 评估板***](./doc/pic/BL618-version.jpg)  
版本号位于 PCB 正面右上角，电源指示灯上方。

### v1.1 与 v1.0

- v1.1.2 ：将调试器板（BL616）与主板（BL618）的 5V 电源连通。
- v1.1.1 ：调试器板（BL616）与主板（BL618）的 5V 电源相互独立。
- v1.0.0 : 早期版本，引出 IO 与 v1.1 版不同。不能使用 v1.1的拓展板。

## 引脚分配

| BL618 引脚号 | 功能                   | 备注                                                 |
| ------------ | ---------------------- | ---------------------------------------------------- |
| GPIO0        | JTAG_TMS               | CK-Link 调试接口。作为 GPIO 使用时，不建议用作输入口 |
| GPIO1        | JTAG_TCK               | CK-Link 调试接口。作为 GPIO 使用时，不建议用作输入口 |
| GPIO2        | JTAG_TDO/Link_DTR/Boot | CK-Link 调试接口。作为 GPIO 使用时，不建议用作输入口 |
| GPIO3        | JTAG_TDI               | CK-Link 调试接口。作为 GPIO 使用时，不建议用作输入口 |
| GPIO4        | Flash_CS               | 连接到Flash,不可作它用                               |
| GPIO5        | Flash_D1               | 连接到Flash,不可作它用                               |
| GPIO6        | Flash_D2               | 连接到Flash,不可作它用                               |
| GPIO7        | Flash_D0               | 连接到Flash,不可作它用                               |
| GPIO8        | Flash_CLK              | 连接到Flash,不可作它用                               |
| GPIO9        | Flash_D3               | 连接到Flash,不可作它用                               |
| GPIO10       | SDH_DAT1               | SD 卡 D1                                             |
| GPIO11       | SDH_DAT0               | SD卡使用                                             |
| GPIO12       | SDH_CLK                | SD卡使用                                             |
| GPIO13       | SDH_CMD                | SD卡使用                                             |
| GPIO14       | SDH_DAT3               | SD 卡 D3,拓展板中用作 I2C SCL                        |
| GPIO15       | SDH_DAT2               | SD 卡 D2,拓展板中用作 I2C SDA                        |
| GPIO16       | 32768Hz晶振            | 作为 GPIO 使用时，不建议用作输入口                   |
| GPIO17       | 32768Hz晶振            | 作为 GPIO 使用时，不建议用作输入口                   |
| GPIO18       |                        |                                                      |
| GPIO19       |                        |                                                      |
| GPIO20       |                        |                                                      |
| GPIO21       | Link_RX                | 调试串口                                             |
| GPIO22       | Link_TX                | 调试串口                                             |
| GPIO23       |                        |                                                      |
| GPIO24       |                        |                                                      |
| GPIO25       |                        |                                                      |
| GPIO26       |                        |                                                      |
| GPIO27       |                        |                                                      |
| GPIO28       |                        |                                                      |
| GPIO29       |                        |                                                      |
| GPIO30       |                        |                                                      |
| GPIO31       |                        |                                                      |
| GPIO32       | LED                    | 用户LED，低电平亮                                    |
| GPIO33       | KEY                    | 用户按键，按下为低电平，松开悬空                     |
| GPIO34       |                        |                                                      |

## CK-Link 固件

- [***BL618评估板 CK-Link 固件 f69076***](./bl616_cklink_whole_img_f69076.bin) ：调试速度更快。***（注意，此版固件下，BL618-G0 的 JTAG口无法用作GPIO。配合BL618-G0 Camera+Audio【摄像头+音频拓展板】使用时，音频部分无法使用）***
- [***BL618评估板 CK-Link 固件 64f497***](./bl616_cklink_whole_img_64f497.bin) ：【推荐】v1.1.2 版硬件的默认出厂固件，兼容 BL618-G0 Camera+Audio【摄像头+音频拓展板】

## 开发资源

- [***BL618 评估板原理图***](./SCH-BL618_eval_board-v1.1.2.pdf)
- [***BL618 评估板尺寸位号图***](./Dimension-BL618_eval_board-v1.1.2.pdf)
- [***BL618 评估板 3D 模型***（仅供参考，不能保证元件模型的准确性）](./3d_bl618eval_v1.1.2_%E4%BB%85%E4%BE%9B%E5%8F%82%E8%80%83.step)
- [***BL616/BL618 数据手册***](./BL616_BL618_DS_zh_CN_1.4(open).pdf)
- [***BL616/BL618 Datasheet（BL616/BL618 英文数据手册）***](./BL616_BL618_DS_en_1.4(open).pdf)
- [***BL616/BL618 参考手册***](./BL616_BL618_RM_zh_CN_0.91_for_verimake.pdf)
- [***软件开发包-bouffalo_sdk_VMG0***](https://gitee.com/verimaker/bouffalo_sdk_vmg0)***（推荐使用）***
- [***软件开发包-bl_mcu_sdk(博流)***](https://gitee.com/bouffalolab/bl_mcu_sdk)***（非板级支持，部分例程需修改引脚配置）***
  